var express = require('express');
var port = 3000;
var app = express();

var path = require('path');
app.use(express.static(path.join(__dirname, '/public/')));

app.listen(port, function(){
  console.log('The app is running on localhost port: ' + port);
});
