var firebase = require('firebase');
var $ = require('jquery');

// connect to firebase
firebase.initializeApp({
    apiKey: "AIzaSyCEIk4hfoynBcir25fA1cCdqXzwqehxcP8",
    authDomain: "turntable-f9e43.firebaseapp.com",
    databaseURL: "https://turntable-f9e43.firebaseio.com",
    storageBucket: "turntable-f9e43.appspot.com",
    messagingSenderId: "727372071047"
});

// Firebase reference object
var ref = firebase.database().ref("characters/");

var turnOrder = []; // To hold the final turn order by initative roll

ref.orderByChild("initRoll").on("value", function(snap){
  // Empty turn order array
  turnOrder = [];
  snap.forEach(function(childSnap){
    //Generate placeholder character object with name and inititive roll fields
    var characterObj = {initRoll:0, name:""};
    // Get character object properties from fireBase
    characterObj.initRoll = childSnap.val().initRoll;
    characterObj.name = childSnap.val().name;
    //Populate turnorder array with characters from fireBase
    turnOrder.push(characterObj);

  //  console.log(childSnap.val());
  });

// Reverse turnorder due to firebase not having descending sort option
turnOrder.reverse();
console.log("first player is now: " + turnOrder[0].name);

new Vue({
  el: '#firstPlayer',
  data: {
    message: turnOrder[0].name
  }
})

});
